<?php
class Newsletter_Widget extends WP_Widget {
	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_newsletter',
			'description' => __( 'A newsletter form for your site.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'newsletter', __( 'Newsletter' ), $widget_ops );
	}

	/**
	 * Outputs the content for the current Newsletter widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Newsletter widget instance.
	 */
	public function widget( $args, $instance ) {	
	//echo 'wat';	
		/*echo '
		<h3>' . $this->get_settings()[2]['title'] . '</h3>
            <p>' . $this->get_settings()[2]['text'] . '</p>
            <div id="newsletter">
                <form action="#" method="get">
                  <input type="text" value="' . $this->get_settings()[2]['input-placeholder'] . '" name="email_newsletter" id="email_newsletter" title="email_newsletter" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
                  <input type="submit" name="subscribe" value="' . $this->get_settings()[2]['button-text'] . '" alt="Subscribe" id="subscribebtn" title="Subscribe" class="subscribebtn"  />
                </form>
                <div class="cleaner"></div>
            </div>';*/
		//print_r($this->get_settings()[2]);
	}

	/**
	 * Outputs the settings form for the Newsletter widget.
	 *
	 * @since 2.8.0
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, 
			array( 
				'title' => '',
			) 
		);
		$title = $instance['title'];
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input 
			class="widefat" 
			id="<?php echo $this->get_field_id('title'); ?>" 
			name="<?php echo $this->get_field_name('title'); ?>" 
			type="text" 
			value="<?php echo esc_attr($title); ?>" />
		</p>
		<?php
	}

	/**
	 * Handles updating settings for the current Search widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args((array) $new_instance, array( 
				'title' => '',
			));
			
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		
		return $instance;
	}

}
