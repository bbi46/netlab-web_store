<?php
/* Template Name: product-page */
get_header();
echo '<div id="templatemo_main_top"></div><div id="templatemo_main">';
echo do_shortcode('[product_slider]');
get_sidebar(); 
the_content(); 
echo '</div>';
get_footer();