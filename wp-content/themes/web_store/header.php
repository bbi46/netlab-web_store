<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Web Store Theme - Free CSS Templates</title>
<meta name="keywords" content="web store, free templates, website templates, CSS, HTML" />
<meta name="description" content="Web Store Theme - free CSS template provided by templatemo.com" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/css/templatemo_style.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/css/styles.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/css/web_store.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/css/ddsmoothmenu.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/css/lightbox.css'; ?>" />
<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/jquery.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/navigation.js'; ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('p:empty').hide();	
});
ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
});

</script>


<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/skip-link-focus-fix.js'; ?>"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/mootools-1.2.1-core.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/mootools-1.2-more.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/slideitmoo-1.1.js"></script>
<script language="javascript" type="text/javascript">
	window.addEvents({
		'domready': function(){
			/* thumbnails example , div containers */
			new SlideItMoo({
						overallContainer: 'SlideItMoo_outer',
						elementScrolled: 'SlideItMoo_inner',
						thumbsContainer: 'SlideItMoo_items',		
						itemsVisible: 5,
						elemsSlide: 2,
						duration: 200,
						itemsSelector: '.SlideItMoo_element',
						itemWidth: 171,
						showControls:1});
		},
		
	});

	function clearText(field)
 	{
		if (field.defaultValue == field.value) field.value = '';
		else if (field.value == '') field.value = field.defaultValue;
	}
</script>

</head>
<?php 
global $posts;
$pSlug = $posts[0]->post_name;
?>
<body id="<?php echo $pSlug == 'home' || $pSlug == 'sample-page' ? 'home' : 'subpage'; ?>">
<?php 
do_action('wp_head'); 
?>
<div id="templatemo_wrapper">
	<div id="templatemo_header">
		<?php 
			$logo_img = fw_get_db_settings_option('logo-img')['url'] ? fw_get_db_settings_option('logo-img')['url'] : '';
			$logo_text = fw_get_db_settings_option('logo-text') ? fw_get_db_settings_option('logo-text') : '';
			$custom_logo_id = get_theme_mod( 'custom_logo' );
			
			$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				if ( !empty( $logo_img ) ) {
					echo '
					<style>
						#site_title { float: left; margin-top: 30px; }
						#site_title h1 { margin: 0; padding: 0 }
						#site_title h1 a { 
							display: block; 
							width: 120px; 
							padding: 35px 0 0 85px; 
							font-size: 12px; 
							color: #999; 
							text-align: left; 
							background: url('. esc_url( $logo_img ) .') no-repeat top left 
						}
					</style>
					<div id="site_title"><h1><a href="http://www.templatemo.com">' . $logo_text . '</a></h1></div>
					';
				}
				else {
					echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
				} 
		?>
        <div id="header_right">
            <ul id="language">
            <?php 
            $flagsAr = fw_get_db_settings_option('upload_flags');
            $url = 'url';
            for($mi=0; $mi<count(fw_get_db_settings_option('upload_flags')); $mi++){
            	echo '<li><a><img src="' . $flagsAr[$mi][$url] . '" /></a></li>';
            }
            ?>
            </ul>
            <div class="cleaner"></div>
            <?php
            if(!empty(fw_get_db_settings_option('search_form'))): 

        	?>
            <div id="templatemo_search">
                <form action="#" method="get">
				  <input type="text" value="<?php echo json_decode(fw_get_db_settings_option('search_form')['json'])[0]->options->placeholder; ?>" name="keyword" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
                  <input type="submit" name="Search" value="" alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
                </form>
            </div>
            <?php endif; ?>
         </div> <!-- END -->
    </div> <!-- END of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
		<?php
			wp_nav_menu( array(
                'menu'              => 'navigation',
                'theme_location'    => 'web_store',
                'depth'             => 2,
                'container'         => '',
                'container_class'   => '',
                'container_id'      => '',
                'menu_class'        => '',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
                );
		?>
        <br style="clear: left" />
    </div> <!-- end of templatemo_menu -->
    <div class="cleaner h20"></div>