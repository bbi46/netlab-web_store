<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
    <?php get_sidebar(); ?>
    <div id="content">
		<table width="700px" cellspacing="0" cellpadding="5">
       	    <tr bgcolor="#CCCCCC">
            	<th width="220" align="left"><?php esc_html_e( 'Image', 'woocommerce' ); ?> </th> 
            	<th width="180" align="left"><?php esc_html_e( 'Description', 'woocommerce' ); ?> </th> 
           	  	<th width="100" align="center"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?> </th> 
            	<th width="60" align="right"><?php esc_html_e( 'Price', 'woocommerce' ); ?> </th> 
            	<th width="60" align="right"><?php esc_html_e( 'Total', 'woocommerce' ); ?> </th> 
            	<th width="90"> </th>
				<th class="product-remove">&nbsp;</th>
            </tr>
            <?php
			$total = [];
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
				//echo '<pre>'; print_r($cart_item); echo '</pre>';
				$total[] = $cart_item['line_total'];
				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr>
						<td class="product-thumbnail"><?php
						//$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
						//echo '<pre>'; print_r($_product->get_gallery_attachment_ids()); echo '</pre>';
						$thumbnail = '<img src="' . wp_get_attachment_url( $_product->get_gallery_attachment_ids()[0] ) . '" />';
						if ( ! $product_permalink ) {
							echo $thumbnail;
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
						}
						?></td>

						<td><?php
						if ( ! $product_permalink ) {
							echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
						} else {
							echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
						}

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item );

						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
						}
						?></td>
						<td><?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input( array(
								'input_name'    => "cart[{$cart_item_key}][qty]",
								'input_value'   => $cart_item['quantity'],
								'max_value'     => $_product->get_max_purchase_quantity(),
								'min_value'     => '0',
								'product_name'  => $_product->get_name(),
							), $_product, false );
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
						?></td>
						<td>
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							?>
						</td>
						<td>
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							?>
						</td>
						<td align="center">  
							<?php
								// @codingStandardsIgnoreLine
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s"><img src="' . get_template_directory_uri() . '/images/remove_x.gif" alt="remove" />
									<br />Remove</a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
							?>
						</td>
					</tr>
					<?php
				}
			}
			?>
			<tr>
				<td colspan="3" align="right"  height="40px">
					<?php echo fw_get_db_settings_option('update-cart-message'); ?>
					<a href="/cart">
						<strong><?php echo __( 'Update', 'woocommerce' ); ?></strong>
						</a>&nbsp;&nbsp;
				</td>
				<td align="right" style="background:#ccc; font-weight:bold"> 
					<?php echo __( 'Total', 'woocommerce' ); ?>
				</td>
				<td align="right" style="background:#ccc; font-weight:bold">
					<?php echo get_woocommerce_currency_symbol() . array_sum($total); ?> </td>
				<td style="background:#ccc; font-weight:bold"> </td>
			</tr>
        </table>
    <div style="float:right; width: 215px; margin-top: 20px;">
        <div class="checkout"><a href="/checkout" class="more"><?php echo __('Proceed to Checkout', 'woocommerce'); ?></a></div>
        <div class="cleaner h20"></div>
        <div class="continueshopping"><a href="javascript:history.back()" class="more"><?php echo __('Continue Shopping', 'woocommerce'); ?></a></div>
    </div>
</div>
<div class="cleaner"></div>
	<?php //do_action( 'woocommerce_before_cart_table' ); ?>		
			<?php //do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			
			$gateways = WC()->payment_gateways->get_available_payment_gateways();
			$enabled_gateways = [];

			if( $gateways ) {
				foreach( $gateways as $gateway ) {
					if( $gateway->enabled == 'yes' ) {
						$enabled_gateways[] = $gateway;
					}
				}
			}

 // Should return an array of enabled gateways
			//echo '<pre>'; print_r( $enabled_gateways ); echo '</pre>';
			?>

			<?php //do_action( 'woocommerce_cart_contents' ); ?>

			
			<?php //do_action( 'woocommerce_after_cart_contents' ); ?>
	<?php //do_action( 'woocommerce_after_cart_table' ); ?>
	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		//do_action( 'woocommerce_cart_collaterals' );
	?>
<?php //do_action( 'woocommerce_after_cart' ); ?>
