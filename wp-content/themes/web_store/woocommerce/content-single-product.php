<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
//echo '<pre>'; print_r($product); echo '</pre>';

/**
 * Hook Woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="col col_13 no_margin_right">
		<table>
			<?php
				$currency = get_woocommerce_currency_symbol();
				echo get_cart_image_styling();
				echo '<tr><td height="30">Price:</td><td>';
				echo $currency.$product->get_price();
				echo '</td></tr>';
				echo '<tr><td height="30">Availability:</td><td>';
				if($product->is_in_stock()){
					echo __('In Stock', 'woocommerce');
				}
				else {
					echo __('Out of stock', 'woocommerce');
				}
				echo '</td></tr>';
				echo '<tr><td height="30">Model:</td><td>';
				$customFields = get_field_objects($product->get_id());
				echo $customFields['model']['value'];
				echo '</td></tr>';
				echo '<tr><td height="30">Manufacturer:</td><td>';
				if(!empty($customFields['manufacturer']))
					echo $customFields['manufacturer']['value'];
				echo '</td></tr>';
				echo '<tr><td height="30">Quantity</td><td>';
				echo quantity_inputs_for_woocommerce_loop_add_to_cart_link('',$product);
				echo '</td></tr></table>
				<div class="cleaner h20"></div>
				<a href="/cart" class="add_to_cart">' . fw_get_db_settings_option('add-to-cart-text') . '</a>';
				?>
				<form class="cart" action="<?php echo esc_url( get_permalink() ); ?>" method="post" enctype='multipart/form-data'>
					<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
				</form>
				<?php
				echo '</div><div class="cleaner h30"></div>';
				echo '<h5><strong>' . __('Product Description', 'woocommerce') . '</strong></h5>';
				echo '<p>' . $product->get_short_description() . '</p>';
				
				$args = array(
					'post_type' => 'product',
					'order' => 'asc',
					'numberposts' => '3'
				); 
				$products = get_posts($args);
				echo '<h4>Other Products</h4>';
				for($p=0; $p<count($products); $p++) {
				$pd = wc_get_product($products[$p]->ID);
				$i = wp_get_attachment_image_src($products[$p]->ID);
				$am = get_attached_media('', $products[$p]->ID);
				//echo '<pre>'; print_r($am); echo '</pre>';
				$guids = [];
				foreach($am as $amm){
					$attachmentId = $amm->ID;
					$attachmentMeta = wp_get_attachment_metadata($attachmentId);
					if($attachmentMeta['width'] == '200'){
						$guids[] = $amm->guid;
					}
					else {
						$guids[] = '';
					}
				}
				$img = $guids[0];
			?>
        	<div class="col col_14 product_gallery">
            	<a href="/product/<?php echo @$pd->slug; ?>"><img src="<?php echo $img; ?>" alt="Product 01" /></a>
                <h3><?php echo @$pd->name; ?></h3>
                <p class="product_price"><?php echo $currency . ' ' . @$pd->price; ?></p>
                <a href="/cart" class="add_to_cart">
				<?php echo fw_get_db_settings_option('add-to-cart-text'); ?>
				</a>
			</div>  
			<?php } 
			//do_action( 'woocommerce_single_product_summary' );
			echo '<a href="#" class="more float_r">View all</a>
            
            <div class="cleaner"></div>
        </div> <!-- END of content -->
        <div class="cleaner"></div>
		</div> <!-- END of main -->';