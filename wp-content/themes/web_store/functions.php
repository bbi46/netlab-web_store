<?php
/**
 * web_store functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package web_store
 */

if ( ! function_exists( 'web_store_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function web_store_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on web_store, use a find and replace
		 * to change 'web_store' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'web_store', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'web_store' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'web_store_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'web_store_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function web_store_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'web_store_content_width', 640 );
}
add_action( 'after_setup_theme', 'web_store_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function web_store_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'web_store' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'web_store' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'web_store_widgets_init' );

add_filter( 'get_custom_logo', 'change_logo_class' );


function change_logo_class( $html ) {

    $html = str_replace( 'custom-logo', 'your-custom-class', $html );
    $html = str_replace( 'custom-logo-link', 'your-custom-class', $html );

    return $html;
}
/**
 * Enqueue scripts and styles.
 */
function web_store_scripts() {
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'web_store_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

require get_template_directory() . '/tgm/web_store.php';
class wp_bootstrap_navwalker extends Walker_Nav_Menu {

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat( "\t", $depth );
        $output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\">\n";
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';


        if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
            $output .= $indent . '<li role="presentation" class="divider">';
        } else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
            $output .= $indent . '<li role="presentation" class="divider">';
        } else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
            $output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
        } else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
            $output .= $indent . '<li role="presentation" class="disabled"><a href="' . $item->url . '">' . esc_attr( $item->title ) . '</a>';
        } else {

            $class_names = $value = '';

            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            $classes[] = 'menu-item-' . $item->ID;

            $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

            if ( $args->has_children )
                $class_names .= ' drop-down-menu';

            if ( in_array( 'current-menu-item', $classes ) )
                $class_names .= 'active';

            $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

            $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
            $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

            $output .= $indent . '<li' . $id . $value . $class_names .'>';

            $atts = array();
            $atts['title']  = ! empty( $item->title )   ? $item->title  : '';
            $atts['target'] = ! empty( $item->target )  ? $item->target : '';
            $atts['rel']    = ! empty( $item->xfn )     ? $item->xfn    : '';


            if ( $args->has_children && $depth === 0 ) {
                $atts['href']           = $item->url;
                $atts['data-toggle']    = 'dropdown';
                $atts['class']          = 'dropdown-toggle';
                $atts['role']           = 'button';
                $atts['aria-haspopup']  = 'true';
                $atts['aria-expanded']  = 'false';



            } else {
                $atts['href'] = ! empty( $item->url ) ? $item->url : '';
            }

            $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

            $attributes = '';
            foreach ( $atts as $attr => $value ) {
                if ( ! empty( $value ) ) {
                    $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                    $attributes .= ' ' . $attr . '="' . $value . '"';
                }
            }

            $item_output = $args->before;
            if(strpos($class_names, 'current_page_item')){
                if(strpos($attributes, 'class')){
                    $attributes = str_replace('class="', 'class="selected ', $attributes);
                }
                else {
                    $attributes .= ' class="selected"';
                }
            }

            if ( ! empty( $item->attr_title ) )
                $item_output .= '<a'. $attributes .'><span class="glyphicon ' . esc_attr( $item->attr_title ) . '"></span>&nbsp;';
            else
                $item_output .= '<a'. $attributes .'>';
            $item_output .='<span>';
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            //$item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="caret"></span></a>' : '</a>';
            $item_output .='</span>';
            $item_output .= ( $args->has_children && 0 === $depth ) ? ' </a>' : '</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }
    }


    public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element )
            return;

        $id_field = $this->db_fields['id'];


        if ( is_object( $args[0] ) )
           $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }


    public static function fallback( $args ) {
        if ( current_user_can( 'manage_options' ) ) {

            extract( $args );

            $fb_output = null;

            if ( $container ) {
                $fb_output = '<' . $container;

                if ( $container_id )
                    $fb_output .= ' id="' . $container_id . '"';

                if ( $container_class )
                    $fb_output .= ' class="' . $container_class . '"';

                $fb_output .= '>';
            }

            $fb_output .= '<ul';

            /*if ( $menu_id )
                $fb_output .= ' id="' . $menu_id . '"';

            if ( $menu_class )
                $fb_output .= ' class="' . $menu_class . '"';*/

            $fb_output .= '>';
            $fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Add a menu</a></li>';
            $fb_output .= '</ul>';

            if ( $container )
                $fb_output .= '</' . $container . '>';

            echo $fb_output;
        }
    }
}

function content_middle_img(){
    $img = get_the_post_thumbnail_url(2);
            echo '<img src="' . $img . '" />';
}

add_shortcode('content_middle_img', 'content_middle_img');

function get_slider (){
    $args = array(
            'post_type'=> 'fw-slider',
            'order'    => 'ASC'
        );      

        $sliderP = get_posts($args);
        $oslider = get_post_meta($sliderP[0]->ID, 'fw_options');
        $slider = 
            '
            <div id="product_slider">
                <div id="SlideItMoo_outer" style="width: 905px;">   
                    <div id="SlideItMoo_inner" style="width: 855px;">           
                        <div id="SlideItMoo_items">';
                            $slidesAr = $oslider[0]['custom-slides'];
                                for($si=0; $si<count($oslider[0]['custom-slides']); $si++){
									$product_info = get_posts(array
									(
										'post_name'   => $oslider[0]['custom-slides'][$si]['title'],
										'post_type'   => 'product',
										'numberposts' => 1,
									));
									//echo '<pre>'; print_r($prouduct_info); echo '</pre>';									
									$slider .= '<div class="SlideItMoo_element">
											<a href="/product/' . $oslider[0]['custom-slides'][$si]['title'] . '" target="_parent">
											<img src="' . $oslider[0]['custom-slides'][$si]['multimedia']['image']['src']['url'] . '" alt="product' . $product_info[0]->ID . '" />
											</a>
											</div>';  
                            }
                        $slider .= '</div>          
                    </div>
                    <div class="SlideItMoo_forward"></div>
                    <div class="SlideItMoo_back"></div>
                </div>
                <div class="cleaner"></div>
            </div>';

    return $slider;
}

add_shortcode('product_slider', 'get_slider');
$args = array(
    'before_widget' => '<div class="widget %s">', 
    'after_widget' => '</div>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>'
    );
$instance = array(
    'title' => 'Title',
    'text' => 'Text'
    );
require_once("class-newsletter-widget.php");
register_widget("Newsletter_Widget");
the_widget( 'Newsletter_Widget', $instance, $args );
function getProductsByCategories(){
    $number = 3;
    $ids = [21,22,24];
 $args = array(

   'number'     => $number,

   'orderby'   => 'title',

   'order'     => 'ASC',

   'hide_empty' => true,

   'include'   => $ids

);

$product_categories = get_terms( 'product_cat', $args );

$count = count($product_categories);

if ( $count > 0 ){
echo '<div id="content">';
   foreach ( $product_categories as $product_category ) {
    // get_term_link( $product_category ) - category link
       echo '
            <h2>' . $product_category->name . '</h2>';
       $args = array(

           'posts_per_page' => -1,

           'tax_query' => array(

              'relation' => 'AND',

               array(

                   'taxonomy' => 'product_cat',

                   'field' => 'slug',

                   // 'terms' => 'white-wines'

                   'terms' => $product_category->slug

                )

           ),

           'post_type' => 'product',

           'orderby' => 'title,'

       );

$products = new WP_Query( $args );
$currency = get_woocommerce_currency_symbol();
$cartImg = fw_get_db_settings_option('cart_image');
		//echo "<pre>";
        //echo "</pre>";
		echo 
		'<style>
		.add_to_cart {
		display: inline-block;
		padding-right: 30px;
		background: url(' . $cartImg['url'] . ') no-repeat right center;
		}
		</style>';
$postsCount = 0;
       while ( $products->have_posts() ) {
           $pd = wc_get_product($products->posts[$postsCount]->ID);
           $products->the_post();
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $products->posts[$postsCount]->ID ), 'single-post-thumbnail' );
           echo '<div class="col col_14 product_gallery">';
           echo '<a href="' . $products->posts[$postsCount]->guid . '"><img src="' . $image[0] . '" width="200px;" alt="Product 01" /></a>';
           echo '<h3>' . $products->posts[$postsCount]->post_title . '</h3>';
           echo '<p class="product_price">' . $currency . ' ' . @$pd->price . '</p>';
           echo '<a href="/cart" class="add_to_cart"  onclick="return preventHref(event);" id="' 
					. $products->posts[$postsCount]->ID . '">' . fw_get_db_settings_option('add-to-cart-text') . '</a>';
		   get_add_to_cart_form($products->posts[$postsCount]->ID);
           echo '<script type="text/javascript" src="' . get_template_directory_uri() . '/js/web_store.js' . '"></script></div>';

            $postsCount++;   
       }
       echo '<a href="#" class="more float_r">View all</a>
            <div class="cleaner h50"></div>';
    }
    echo '</div><div class="cleaner"></div>';
}
}
add_shortcode('products-by-categories', 'getProductsByCategories'); 

function get_page_posts($posts_ids){
    $idsAr  = [];
    if(strpos($posts_ids['posts_ids'], ',')){
        $exIdsAr = explode(',', $posts_ids['posts_ids']);
        $idsAr   = [];
        for($i=0; $i<count($exIdsAr); $i++){
            $idsAr[] = $exIdsAr[$i];
        }
    }
    else {
        $idsAr[]  = $posts_ids['posts_ids'];
    }
   
    $args = array(
        'posts_per_page'   => -1,
        'orderby'          => 'ID',
        'order'            => 'ASC',
        'post_type'        => 'post',
        'post__in'         => $idsAr
    );
    $posts = get_posts($args);
    for($pi=0; $pi<count($posts); $pi++){
        if($posts[$pi]->post_title){
        echo '<h3>' . $posts[$pi]->post_title . '</h3>';    
    }
        if($posts[$pi]->post_content){
        echo $posts[$pi]->post_content;    
    }
        echo '<div class="cleaner"></div>';    
    }
    
}

add_shortcode('get-page-posts', 'get_page_posts'); 

function get_billing_form(){
	$ch = new WC_Checkout;
	echo '<div class="col col_13 checkout">'
		. $ch->get_checkout_fields()['billing']['billing_card_number']['label']
		. '<input type="text"  style="width:300px;"  />'
		. $ch->get_checkout_fields()['billing']['billing_address_1']['label']
        . '<input type="text"  style="width:300px;"  />'
		. $ch->get_checkout_fields()['billing']['billing_city']['label']
		. '<input type="text"  style="width:300px;"  />'
		. $ch->get_checkout_fields()['billing']['billing_country']['label']
        . '<input type="text"  style="width:300px;"  />
            </div>'
		. '<div class="col col_13 checkout">'
		. $ch->get_checkout_fields()['billing']['billing_email']['label']
		. '<input type="text"  style="width:300px;"  />'
		. $ch->get_checkout_fields()['billing']['billing_phone']['label']
        . '<input type="text"  style="width:300px;"  />
            </div>
            <div class="cleaner h50"></div>';
}

add_shortcode('billing-form', 'get_billing_form');

function shopping_cart(){

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$total = [];
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		//echo '<pre>'; print_r($cart_item); echo '</pre>';
		$total[] = $cart_item['line_total'];
	}
	$gateways = WC()->payment_gateways->get_available_payment_gateways();
	$enabled_gateways = [];
		if( $gateways ) {
			foreach( $gateways as $gateway ) {
				if( $gateway->enabled == 'yes' ) {
					$enabled_gateways[] = $gateway;
				}
			}
		}
		// Should return an array of enabled gateways
		//echo '<pre>'; print_r( $enabled_gateways ); echo '</pre>';
	echo '<h4>' 
		. strtoupper(esc_html( 'Total', 'woocommerce' ))
		. ': <strong>' . get_woocommerce_currency_symbol().array_sum($total)
		. '</strong></h4>';
		$acceptedMessage = fw_get_db_settings_option('i-accepted-message');
		$termsOfUseMessage = fw_get_db_settings_option('terms-of-use-message');
	?>
<form class="woocommerce-cart-form" id="form-cart" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<p><input type="checkbox" /><?php echo $acceptedMessage; ?> <a href="#"><?php echo $termsOfUseMessage; ?></a></p>
		<table style="border:1px solid #CCCCCC;" width="100%">
		<tr>
			<td height="80px"> <img src="<?php echo get_template_directory_uri() . '/' . $enabled_gateways[0]->settings['image_url']; ?>" alt="paypal" /></td>
			<td width="400px;" style="padding: 0px 20px;"><?php echo $enabled_gateways[0]->settings['description']; ?>
			</td>
			<td><a href="#form-cart" class="more"><?php echo strtoupper($enabled_gateways[0]->settings['title']); ?></a></td>
		</tr>
			<tr>
				<td  height="80px"><img src="<?php echo fw_get_db_settings_option('twocheckout-icon')['url']; ?>" alt="paypal" />
				</td>
				<td  width="400px;" style="padding: 0px 20px;"><?php echo $enabled_gateways[2]->description; ?>
				</td>
				<td><a href="#form-cart" class="more"><?php echo strtoupper($enabled_gateways[2]->title); ?></a></td>
			</tr>
		<tr id="cart-button"  style="display: none;" >
			<td colspan="6" class="actions">
				<button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Go to cart', 'woocommerce' ); ?></button>
				<?php 
					do_action( 'woocommerce_cart_actions' );  
					wp_nonce_field( 'woocommerce-cart' ); 
				?>
			</td>
		</tr>
	</table>
</form>
<script>
jQuery('input[type="checkbox"]').change(function(){
	if(this.checked){
		jQuery('#tuer').hide();
	}
	else {
		jQuery('#tuer').show();
		jQuery('#cart-button').hide();
	}
});
jQuery('.more').click(function(){
	var chb = jQuery('input[type="checkbox"]');
	if(chb[0].checked){
		jQuery('#cart-button').show();
	}
	else {
		jQuery('<p id="tuer" style="color: red;">You must accept terms of use</p>').insertAfter(jQuery(".woocommerce-cart-form") );
		jQuery('#cart-button').hide();
	}
});
</script>
<?php } 
add_shortcode('shopping_cart', 'shopping_cart');

add_action('woocommerce_breadcrumb', 'remove_breadcrumbs', '');

apply_filters( 'woocommerce_breadcrumb_defaults', 'remove_breadcrumbs' );

function remove_breadcrumbs( $defaults ){
    $defaults = [];
    $defaults['home'] = 'nooooooooooo';
    return $defaults;
}

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce', array(
        'thumbnail_image_width' => 150,
        'single_image_width'    => 300,

        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 4,
            'min_columns'     => 2,
            'max_columns'     => 5,
        ),
    ) );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_action( 'save_post_product', 'auto_add_product_attributes', 50, 3 );
function auto_add_product_attributes( $post_id, $post, $update  ) {

    ## --- Checking --- ##

    // Exit if it's an autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return $post_id;

    // Exit if it's an update
    if( $update )
        return $post_id;

    // Exit if user is not allowed
    if ( ! current_user_can( 'edit_product', $post_id ) )
        return $post_id;

    ## --- The Settings for your product attributes --- ##

    $visible   = ''; // can be: '' or '1'
    $variation = ''; // can be: '' or '1'

    ## --- The code --- ##

    // Get all existing product attributes
    global $wpdb;
    $attributes = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies" );

    $position   = 0;  // Auto incremented position value starting at '0'
    $data       = array(); // initialising (empty array)

    // Loop through each exiting product attribute
    foreach( $attributes as $attribute ){
        // Get the correct taxonomy for product attributes
        $taxonomy = 'pa_'.$attribute->attribute_name;
        $attribute_id = $attribute->attribute_id;

        // Get all term Ids values for the current product attribute (array)
        $term_ids = get_terms(array('taxonomy' => $taxonomy, 'fields' => 'ids'));

        // Get an empty instance of the WC_Product_Attribute object
        $product_attribute = new WC_Product_Attribute();

        // Set the related data in the WC_Product_Attribute object
        $product_attribute->set_id( $attribute_id );
        $product_attribute->set_name( $taxonomy );
        $product_attribute->set_options( $term_ids );
        $product_attribute->set_position( $position );
        $product_attribute->set_visible( $visible );
        $product_attribute->set_variation( $variation );

        // Add the product WC_Product_Attribute object in the data array
        $data[$taxonomy] = $product_attribute;

        $position++; // Incrementing position
    }
    // Get an instance of the WC_Product object
    $product = wc_get_product( $post_id );

    // Set the array of WC_Product_Attribute objects in the product
    $product->set_attributes( $data );

    $product->save(); // Save the product
}

add_filter( 'woocommerce_loop_add_to_cart_link', 'quantity_inputs_for_woocommerce_loop_add_to_cart_link', 10, 2 );
function quantity_inputs_for_woocommerce_loop_add_to_cart_link( $html, $product ) {
	if ( $product && $product->is_type( 'simple' ) && $product->is_purchasable() && $product->is_in_stock() && ! $product->is_sold_individually() ) {
		$html .= '<form action="' . esc_url( $product->add_to_cart_url() ) . '" method="post" enctype="multipart/form-data">';
		$html .= woocommerce_quantity_input( array(), $product, false );
		$html .= '<button type="submit" style="display: none;" class="button alt">' . esc_html( $product->add_to_cart_text() ) . '</button>';
		$html .= '</form>';
	}
	return $html;
}

function get_cart_image_styling(){
	$cartImg = fw_get_db_settings_option('cart_image');
		return 
			'<style>
			.add_to_cart {
			display: inline-block;
			padding-right: 30px;
			background: url(' . $cartImg['url'] . ') no-repeat right center;
			}
			</style>';
}

function get_single_products_category_data(){
	$catU = explode('/', $_SERVER['REQUEST_URI']);
	$category = get_term_by( 'slug', $catU[2], 'product_cat' );
	$cat_id = $category->term_id;
	// get_term_link( $product_category ) - category link
	$number = 1;
    $ids = [$cat_id];
     $args = array(
        'number'     => $number,
        'orderby'   => 'title',
        'order'     => 'ASC',
        'hide_empty' => true,
        'include'   => $ids
    );

    $product_categories = get_terms( 'product_cat', $args );
	return $product_categories[0];
}

function get_add_to_cart_form($product_id){
	echo '
		<form class="cart" action="/cart" method="post" enctype="multipart/form-data">
			<button 
				id="' . $product_id . '"
				type="submit" 
				name="add-to-cart" 
				value="' . esc_attr( $product_id ) . '" 
				class="single_add_to_cart_button button alt"
				style="display: none;">
			</button>
		</form>';
}