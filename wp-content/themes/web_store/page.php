<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package web_store
 */

get_header();
?>
<div id="templatemo_middle">
	<?php the_content(); ?>
	<?php get_sidebar(); ?>
	<?php 
		$args = array(
			'post_type' => 'product',
			'order' => 'asc',
			'numberposts' => '-1'
        ); 
		$products = get_posts($args);
        $currency = get_woocommerce_currency_symbol();
		echo get_cart_image_styling();
	?>
        <div id="content">
			<?php 

    			//$attachment_ids = $product->get_gallery_attachment_ids();
    			//echo '<pre>'; print_r($pI); echo '</pre>';
			for($p=0; $p<count($products); $p++) {
				$pd = wc_get_product($products[$p]->ID);
				//$img = get_the_post_thumbnail_url($products[$p]->ID);
				
				$i = wp_get_attachment_image_src($products[$p]->ID);
				$am = get_attached_media('', $products[$p]->ID);
				//echo 'i--' . $i;
				//echo '<pre>'; print_r($am); echo '</pre>';
				$guids = [];
				foreach($am as $amm){
					$guids[] = $amm->guid;
				}
				$img = $guids[0];
				$terms = get_the_terms( $products[$p]->ID, 'product_cat' );
					foreach ($terms as $term) {
						$product_cat_id = $term->name;
						break;
					}
				if($product_cat_id !== 'Uncategorized'){	
			?>
			<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/web_store.js'; ?>"></script>
        	<div class="col col_14 product_gallery">
            	<a href="/product/<?php echo @$pd->slug; ?>"><img src="<?php echo $img; ?>" alt="Product 01" /></a>
                <h3><?php echo @$pd->name; ?></h3>
                <p class="product_price"><?php echo $currency . ' ' . @$pd->price; ?></p>
                <a href="/cart" class="add_to_cart" onclick="return preventHref(event);" id="<?php echo $products[$p]->ID; ?>">
				<?php echo fw_get_db_settings_option('add-to-cart-text'); ?>
				</a>
				<?php
					global $product;
					get_add_to_cart_form($products[$p]->ID)
				?>
				</div>  
			<?php }} ?>    	
        </div> <!-- END of content -->
        <div class="cleaner"></div>
    </div> <!-- END of main -->
<?php
get_footer();