<?php
/* Template Name: info-pages */
get_header();
echo '<div id="templatemo_main_top"></div>
    <div id="templatemo_main">';
get_sidebar();
global $post;
$page=get_post($post->ID);
$shortcode = [];
if(preg_match('/\[get-page-posts posts_ids=[\d,]+\]/', $page->post_content, $shortcode)){
	$exPageContent = explode($shortcode[0], $page->post_content);
	echo $exPageContent[0];
$posts=[];
preg_match('/[\d,]+/', $shortcode[0], $posts);
$exPosts=explode(',', $posts[0]);
$args = [
'post__in' => $exPosts,
'order'=>'asc',
];
$allPosts = get_posts($args);
//print_r($allPosts);
for($pi=0;$pi<count($allPosts);$pi++){
echo '<h3>' . $allPosts[$pi]->post_title . '</h3>';
$shortcodes = [];
if(preg_match_all('/\[[a-z-_]+\]/', $allPosts[$pi]->post_content, $shortcodes)){
	for($si=0; $si<count($shortcodes[0]); $si++){
		echo do_shortcode($shortcodes[0][$si]);
	}
}
else {
	echo $allPosts[$pi]->post_content;
}
}
echo $exPageContent[1];
}
else {
	echo $page->posts_content;
}
//echo '<pre>';
//print_r($_REQUEST);
//print_r(WC()->customer->data['billing']);
//$ch = new WC_Checkout;
//print_r($ch->get_checkout_fields()['billing']);
//echo '</pre>';
echo '</div>';
get_footer();