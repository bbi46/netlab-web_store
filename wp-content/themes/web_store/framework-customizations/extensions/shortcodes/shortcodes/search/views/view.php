<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
?>
<div class="fw-search_form">
	<?php foreach ( fw_akg( 'form', $atts, array() ) as $input ) : ?>
		<h3 class="fw-search_form-title"><?php echo $input['tab_title']; ?></h3>
	<?php endforeach; ?>
	<input type="text" value="search" />
</div>
