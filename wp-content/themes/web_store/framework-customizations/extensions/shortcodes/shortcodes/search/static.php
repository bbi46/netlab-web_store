<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');
wp_enqueue_script(
	'fw-shortcode-search_form',
	$shortcodes_extension->get_declared_URI('/shortcodes/search/static/js/scripts.js'),
	array('jquery-ui-accordion'),
	false,
	true
);
wp_enqueue_style(
	'fw-shortcode-search_form',
	$shortcodes_extension->get_declared_URI('/shortcodes/search/static/css/styles.css')
);

