<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Search form', 'fw' ),
	'description' => __( 'Add an Search form', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);
