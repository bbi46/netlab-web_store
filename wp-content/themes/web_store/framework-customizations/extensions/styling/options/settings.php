<?php
$options = array(
	'theme_style' => array(
		'label' => false,
		'type'  => 'style',
		/**
		 * Must contain a list of predefined styles
		 * that contains the name, icon and value for each block element
		 */
		'predefined' => $predefined = include_once( 'predefined-styles.php' ),
		/**
		 * Must contain the initial value for each element from each block
		 */
		'value' => $predefined['black']['blocks'],
		/**
		 * Each key contains the necessary settings to stylize one or more blocks of the site
		 */
		'blocks' => array(
			'block-1' => array(
				'title' => __( 'Header', 'fw' ),
				/**
				 * Elements that can be controlled
				 * Allowed elements are: 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'links', 'links_hover', 'background'
				 */
				'elements' => array( 'h1', 'links', 'links_hover', 'background' ),
				'css_selector' => array(
					'#masthead',
					'.primary-navigation .mega-menu',
					'.primary-navigation .mega-col',
					'.primary-navigation .mega-row',
				),
				/**
				 * Additional options that will be displayed in this block, before standard elements
				 */
				'before' => array(
					'menu_border' => array(
						'label' => __('Menu Border', 'fw'),
						'desc'  => false,
						'type'  => 'color-picker',
						'value' => '#cccccc',
					)
				),
				/**
				 * Addition options that will be displayed in this block, after standard elements
				 */
				'after' => array(
					// ...
				)
			),
			'block-2' => array(
				// ...
			),
		),
	)
);
