<?php
if (!defined('FW')) die('Forbidden');

/** @internal */
function _action_theme_fw_ext_forms_include_newsletter_builder_item() {
    require_once dirname(__FILE__) .'/includes/builder-items/newsletter/class-fw-option-type-form-builder-item-newsletter.php';
}
add_action('fw_option_type_form_builder_init', '_action_theme_fw_ext_forms_include_newsletter_builder_item');