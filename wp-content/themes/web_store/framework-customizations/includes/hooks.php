<?php
/** @internal */
function _action_theme_include_custom_option_types() {
    require_once dirname(__FILE__) . '/option-types/color-mega-picker/class-fw-option-type-color-mega-picker.php';
}
add_action('fw_option_types_init', '_action_theme_include_custom_option_types');
add_action('fw_init', '_action_theme_include_custom_option_types');