<?php
$options = array(
    /**
     * When used in Post Options on the first array level
     * the ``box`` container accepts additional parameters
     */
    //'context' => 'normal|advanced|side',
    //'priority' => 'default|high|core|low',
	'tab1' => array(
			'type' => 'tab',
			'context' => 'advanced',
			'priority' => 'core',
			'options' => array(
				'logo-img' => array(
					'type' => 'upload',
					'label' => __('Logo'),
					'desc'  => __('Upload logo image', 'fw'),
					),
				'logo-text' => array(
					'type' => 'text',
					'label' => __(''),
					'desc'  => __('Set logo text', 'fw'),
					),
				'upload_flags' => array(
					'type'  => 'multi-upload',
					'value' => array(
						 'attachment_id' => '11',
						 'url' => ''
					),
					'value' => array(
						 'attachment_id' => '10',
						 'url' => ''
					),
					'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
					'label' => __('Languages flags', 'fw'),
					'desc'  => __('Description', 'fw'),
					'help'  => __('Change flags for language checker', 'fw'),
					/**
					 * If set to `true`, the option will allow to upload only images, and display a thumb of the selected one.
					 * If set to `false`, the option will allow to upload any file from the media library.
					 */
					'images_only' => true,
				),
				'search_form' => array(
					'type'  => 'form-builder',
					'label' => 'Search form',
					'value' => array(
						'options' => array('placeholder' => '')
					),
				),
			),
		'title' => __('Header', '{domain}'),
		//'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
	),
	'tab2' => array(
		'type' => 'tab',
			'context' => 'advanced',
			'priority' => 'core',
			'options' => array(
				'footer_content' => array(
					'type'  => 'page-builder',
					'label' => 'Footer Content',
				),
				),
		'title' => __('Footer', '{domain}'),
		),
	'tab3' => array(
		'type' => 'tab',
			'context' => 'advanced',
			'priority' => 'core',
			'options' => array(
				'newsletter-text' => array(
					'type'  => 'text',
					'label' => 'Newsletter',
					'desc'  => __('Text before form', 'fw'),
				),
				'newsletter-placeholder' => array(
					'type'  => 'text',
					'label' => '',
					'desc'  => __('Placeholder', 'fw'),
				),
				'newsletter-button-text' => array(
					'type'  => 'text',
					'label' => '',
					'desc'  => __('Button Text', 'fw'),
				),
				),
		'title' => __('Newsletter', '{domain}'),
		),
	'tab4' => array(
		'type' => 'tab',
		'context' => 'advanced',
			'priority' => 'core',
			'options' => array(
				'add-to-cart-text' => array(
					'type'  => 'text',
					'label' => 'Add to cart text',
				),
				'cart_image' => array(
					'type'  => 'upload',
					'label' => 'Cart Image',
				),
				'billing_form' => array(
					'type'  => 'form-builder',
					'label' => 'Billing form',
					'value' => array(
						'options' => array('placeholder' => '')
					),
				),
				'product-title' => array(
					'type'  => 'text',
					'label' => 'Product Page Title',
				),
			),
			'title' => __('Products', '{domain}'),
	),
	'tab5' => array(
		'type' => 'tab',
		'context' => 'advanced',
			'priority' => 'core',
			'options' => array(
				'i-accepted-message' => array(
					'type'  => 'text',
					'label' => 'I Accepted Terms Of Use Message',
				),
				'terms-of-use-message' => array(
					'type'  => 'text',
					'label' => 'Terms Of Use Message',
				),
				'twocheckout-icon' => array(
					'type'  => 'upload',
					'label' => '2CO Icon',
				),
			),
			'title' => __('Checkout', '{domain}'),
	),
	'tab6' => array(
		'type' => 'tab',
		'context' => 'advanced',
			'priority' => 'core',
			'options' => array(
				'update-cart-message' => array(
					'type'  => 'text',
					'label' => 'Update Cart Message',
				),
			),
			'title' => __('Cart', '{domain}'),
	)
);
