<?php
$options = array(
    /**
     * When used in Post Options on the first array level
     * the ``box`` container accepts additional parameters
     */
    //'context' => 'normal|advanced|side',
    //'priority' => 'default|high|core|low',
	'tab1' => array(
			'type' => 'tab',
			'context' => 'advanced',
			'priority' => 'core',
			'options' => array(
				'upload_flags' => array(
					'type'  => 'multi-upload',
					'value' => array(
						 'attachment_id' => '11',
						 'url' => ''
					),
					'value' => array(
						 'attachment_id' => '10',
						 'url' => ''
					),
					'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
					'label' => __('Languages flags', 'fw'),
					'desc'  => __('Description', 'fw'),
					'help'  => __('Change flags for language checker', 'fw'),
					/**
					 * If set to `true`, the option will allow to upload only images, and display a thumb of the selected one.
					 * If set to `false`, the option will allow to upload any file from the media library.
					 */
					'images_only' => true,
				),
				'theme-page-builder' => array(
					'label'              => 'Page Builder',
					'desc'               => false,
					'type'               => 'page-builder',
					'editor_integration' => false, // Don't even try to set this to true, you're warned.
					'fullscreen'         => true,
					'template_saving'    => true,
					'history'            => true,
				),
				'search_form' => array(
					'type'  => 'checkbox',
					'value' => true, // checked/unchecked
					'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
					'label' => __('Label', '{domain}'),
					'desc'  => __('Description', '{domain}'),
					'help'  => __('Help tip', '{domain}'),
					'text'  => __('Yes', '{domain}'),
					)
			),
		'title' => __('Language flags', '{domain}'),
		'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
	),
);
