<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package web_store
 */
////echo '<pre>';
//echo $sidebar;
//echo '</pre>';
if ( is_active_sidebar( 'sidebar-1' ) ){
	$args = array(
    'before_widget' => '<div class="widget %s">', 
    'after_widget' => '</div>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>'
    );
$instance = array(
    'title' => 'Title',
    'text' => 'Text'
    );
require_once("product_categories_widget.php");
register_widget("Product_Categories_Widget");
the_widget( 'Product_Categories_Widget', $instance, $args );

}
global $wp_registered_widgets;

echo '<div id="newsletter">
		<h3>' . $wp_registered_widgets['newsletter-2']['name'] . '</h3>
		<p id="before-nl-form">' . fw_get_db_settings_option('newsletter-text')
             .    '</p><form action="#" method="get">
                  <input type="text" value="' . fw_get_db_settings_option('newsletter-placeholder') . '" name="email_newsletter" id="email_newsletter" title="email_newsletter" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
                  <input type="submit" name="subscribe" value="' . fw_get_db_settings_option('newsletter-button-text') . '" alt="Subscribe" id="subscribebtn" title="Subscribe" class="subscribebtn"  />
                </form>
                <div class="cleaner"></div>
            </div>
</div>';